# urzadzenia-mobilne

Projekty wykonane na urządzenia mobilne. Głównie android.

## Spis projektów:

 - [AR w sklepie internetowym](#ar-w-sklepie-internetowym)
 - [Phoenix Contact VR](#phoenix-contact-vr)
 - [Liczenie dystanu](#liczenie-dystansu)

 #
 
 
### AR w sklepie internetowym

Aplikacja wykonana za pomocą technologii [ARCore](https://unity3d.com/partners/google/arcore) w silniu graficznym Unity.
Żeby nie kopiować artykułu, podam odnośnik do strony z informacjami o aplikacji: [EpicVR.pl](https://epicvr.pl/pl/ar-w-sklepie-internetowym-wykorzystaj-potencjal-rzeczywistosci-rozszerzonej/).

<a align='center' href="https://youtu.be/9M1IcP2P1AY" target="_blank"><img src="http://img.youtube.com/vi/9M1IcP2P1AY/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

<a align='center' href="https://youtu.be/dR-DcV4h34M" target="_blank"><img src="http://img.youtube.com/vi/dR-DcV4h34M/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

#

### Phoenix Contact VR

Aplikacja przeznaczona na [Gear VR](https://www.samsung.com/pl/wearables/gear-vr-r322/) ze sterowaniem wzrokiem (gaze pointer). Głównym zadaniem aplikacji jest umożliwienie przeglądania katalogów z produktami, a także prosta minigra.

<a align='center' href="https://youtu.be/jD3uhGP0bzY" target="_blank"><img src="http://img.youtube.com/vi/jD3uhGP0bzY/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

#

### Liczenie dystanu

Aplikacja zrobiona za pomocą narzędzia [Vuforia](https://developer.vuforia.com/) jako assetu do silnika graficznego Unity. Aplikacja czytając grafiki jest w stanie obliczyć dystans pomiędzy punktami. Wstępnie należy jednak skalibrować dystans.


<a align='center' href="https://youtu.be/h3EfyZYdCLk" target="_blank"><img src="http://img.youtube.com/vi/h3EfyZYdCLk/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

